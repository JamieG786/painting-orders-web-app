<!DOCTYPE html>
<?php
//Connect to MySQL

$conn = new mysqli($host, $user, $pass, $dbname);

if ($conn->connect_error) {
    die("Connection Failed");
}
?>

<html>
<head>
    <title>Create Account</title>
    <link rel="stylesheet" href="login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <ul>
        <?php if (isset($_SESSION["loggedin"])) { ?>
            <li><a href="viewordersaccount.php">View Orders</a></li>
            <li><a href="logout.php">Log Out</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>

        <?php } else { ?>
            <li><a href="login.php">Sign In</a></li>
            <li><a href="createaccount.php">Sign Up</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>
        <?php } ?>
    </ul>
</head>
<body>
<h1>Create account</h1>

<?php

$email = $password = $email_err = "";
$validation = "FALSE";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $result = "";
    $email = isset($_POST['email']) ? $conn->real_escape_string($_POST['email']) : "";
    $sql = "SELECT * FROM `accounts` WHERE `email` ='$email'";
    $password = isset($_POST['password']) ? $conn->real_escape_string($_POST['password']) : "";
    $hash = password_hash($password, PASSWORD_DEFAULT);
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $email_err = "This email is already being used";
    } else {
        $sql_insert = "INSERT INTO `accounts` (`id`, `email`, `password`) VALUES (NULL, '$email', '$hash')";
        $insert_result = $conn->query($sql_insert);
        $validation = "TRUE";
    }
}
?>
<div class="grid">
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <?php if ($validation === "FALSE") { ?>
        <div class="boxes">
            <input type="email" placeholder="Email" name="email" value="" required>
            <span class="error">* <?php echo $email_err ?></span>
            <br><br>
            <input type="password" placeholder="Password" name="password" required>
            <span class="error">*</span>
            <br><br>
            <button class="submit" name="submit" type="Submit" value="">Create</button>
            <br><br>
    </form>
    <form action="listart.php">
        <button class="submit" name="back" type="Submit" value="">Back</button>

        <?php } else { ?>
        <h3>Your account has succesfully been created</h3>
            <button class = "submit" name = "login" type = "Submit" value="">Log In</button>
        </form>
</div>

<?php } ?>
</form>
</div>

</body>
</html>

<?php
$join = array("s" => "stay fit", "g" => "get fit", "m" => "meet people", "o" => "other");

$sql = "SELECT * FROM `accounts`";
$result = $conn->query($sql);
if (!$result) {
    die("query failed");
} ?>
        <?php while ($row = $result->fetch_assoc()) { ?>
            <tr>
                <th>Name</th>
                <th>Age</th>
                <th>Reason for Joining</th>
            </tr>
            <tr>
                <td><?php echo $row["name"] ?></td>
                <td><?php echo $row["age"] ?></td>
                <td><?php echo $join[$row["reason"]] ?></td>
            </tr>
        <?php } ?>


<?php
$cookie_name = "user";
$cookie_value = "completed";
setcookie($cookie_name, $cookie_value, time() + (86400 * 40)); // 86400 = 1 day
if(isset($_COOKIE[$cookie_name])) {
    echo "form has already been completed";
}
?>
