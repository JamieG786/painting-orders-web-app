<?php session_start(); ?>
<!DOCTYPE html>
<?php
//Connect to MySQL

$conn = new mysqli($host, $user, $pass, $dbname);

if ($conn->connect_error) {
    die("Connection Failed");
}


$email_err = "";
$validation = "FALSE";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = isset($_POST['email']) ? $conn->real_escape_string($_POST['email']) : "";
    $password = isset($_POST['password']) ? $conn->real_escape_string($_POST['password']) : "";
    $hash = password_hash($password, PASSWORD_DEFAULT);
    $sql = "SELECT * FROM `accounts` WHERE `email` ='$email'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $updatesql = "UPDATE `accounts` SET `password` ='$hash'  WHERE `accounts`.`email` = '$email'";
        $updateresult = $conn->query($updatesql);
        $validation = "TRUE";


    } else {
        $email_err = "We dont have an account in our system with this email address";
    }
}


?>

<html>
<head>
    <title>Forgot Password</title>
    <link rel="stylesheet" href="login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <ul>
        <?php if (isset($_SESSION["loggedin"])) { ?>
            <li><a href="viewordersaccount.php">View Orders</a></li>
            <li><a href="logout.php">Log Out</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>

        <?php } else { ?>
            <li><a href="login.php">Sign In</a></li>
            <li><a href="createaccount.php">Sign Up</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>
        <?php } ?>
    </ul>
</head>
<body>
<h1>Forgot Password</h1>
<div class="grid">
    <?php if ($validation === "FALSE") { ?>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class = "text">
            <p>Please enter the email address you created with your account and then enter your new password.</p>
            </div>
            <div class = "boxes">
            <input type="email" placeholder="Email" name="email" value="" required>
            <span class="error">* <?php echo $email_err ?></span>
            <br><br>
            <input type="password" placeholder="New Password" name="password" required>
            <span class="error">*</span>
            <br><br>
            </div>
            <button class = "submit" name="submit" type="Submit" value="">Change Password</button>
        </form>
        <form action="listart.php">
            <button class = "submit" name="back" type="Submit">Back</button>
        </form>

    <?php } else { ?>
        <form action=listart.php>
            <h3>Password succesfully changed</h3>
            <button class = "submit" name="submit" type="Submit" value="">Log in</button>
        </form>
    <?php } ?>
</div>
</body>
</html>