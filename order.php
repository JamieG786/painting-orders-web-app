<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <title>Order</title>
    <link rel="stylesheet" href="login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <ul>
        <?php if (isset($_SESSION["loggedin"])) { ?>
            <li><a href="viewordersaccount.php">View Orders</a></li>
            <li><a href="logout.php">Log Out</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>

        <?php } else { ?>
            <li><a href="login.php">Sign In</a></li>
            <li><a href="createaccount.php">Sign Up</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>
        <?php } ?>
    </ul>
</head>
<body>
<h1>Order</h1>

<form method="POST" action="orderComplete.php">

    <?php

    //Connect to MySQL

    $conn = new mysqli($host, $user, $pass, $dbname);

    if ($conn->connect_error) {
        die("Connection Failed");
    }
    $id = $conn->real_escape_string($_GET['order']);
    //Issue the query
    $sql = 'SELECT *  FROM `paintings` WHERE `id` = ' . $id;
    $result = $conn->query($sql);

    if (!$result) {
        die("Query failed");
    }
    $row = $result->fetch_assoc();

    $painting_id = $row["id"];
    $painting_name = $row["name"]; ?>
    <div class="grid">
        <div class="text">
            <?php echo "Please enter your details to confirm your order of painting " . $painting_id . ": " . $painting_name;
            ?></div>
        <br><br>
        <div class = "boxes">
        <input type="text" placeholder="Full Name" name="name" required>
        <span class="error">*</span>
        <br><br>
        <input type="text" placeholder="Phone Number" name="number" required>
        <span class="error">*</span>
        <br><br>

            <?php if (isset($_SESSION["email"])) { ?>
                <input type="email" name="emaildisabled" placeholder="<?php echo $_SESSION["email"] ?>" disabled>
                <input type="hidden" name="email" value="<?php echo $_SESSION["email"] ?>">
            <?php } else { ?>
                <input type="email" name="email" placeholder="Email" required>
                <span class="error">*</span>
            <?php } ?>
            <br><br>
            <input type="text" placeholder="Full Address" name="address" required>
            <span class="error">*</span>
        <br><br>
        <input type="hidden" name="painting_id" value="<?php echo $painting_id ?>">
        <input type="hidden" name="painting_name" value="<?php echo $painting_name ?>">

        <input class = "submit" type="submit" value="Submit">


</form>
<form action="listart.php">
    <button class = "submit" name="back" type="Submit" value="">Back</button>
</form>
</div>
</div>
</body>
</html>