<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
    <title>Paintings for Sale</title>
    <link rel="stylesheet" href="main3.css">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <ul>
        <?php if (isset($_SESSION["loggedin"])) { ?>
            <li><a href="viewordersaccount.php">View Orders</a></li>
            <li><a href="logout.php">Log Out</a></li>
            <li class = "title"><a href="listart.php">Art By Cara</a></li>

        <?php } else { ?>
            <li><a href="login.php">Sign In</a></li>
            <li><a href="createaccount.php">Sign Up</a></li>
            <li class = "title"><a href="listart.php">Art By Cara</a></li>
        <?php } ?>
    </ul>
</head>
<body>

<div>
    <form action="order.php" method="get">
        <?php
        //Connect to MySQL

        $conn = new mysqli($host, $user, $pass, $dbname);

        if (isset($_GET['page'])) {
            $pageno = $_GET['page'];
        } else {
            $pageno = 1;
        }
        $limit = 12;
        $offset = ($pageno - 1) * $limit;

        $page_query = "SELECT * FROM `paintings`";
        $page_result = $conn->query($page_query);

        if (!$page_result) {
            die("Query failed");
        }

        $total_records = $page_result->num_rows;
        $total_pages = ceil($total_records / $limit);

        //Issue the query
        $sql = "SELECT * FROM `paintings` LIMIT $offset, $limit";
        $result = $conn->query($sql);

        if (!$result) {
            die("Query failed");
        }
        ?>


        <div class="grid">
        <?php while ($row = $result->fetch_assoc()) { ?>

                <div class="item">
                    <div class="photo"> <?php echo "<img src='data:image/jpeg;base64,".base64_encode($row["image"])."'"?> height = 50 width = 50> </div>
                    <div class = "painting_title"><?php echo $row["name"] ?> </div><br>
                    <div>Height: <?php echo $row["height"] ?>cm</div>
                    <div>Width: <?php echo $row["width"] ?>cm</div>
                    <div class = "painting_price">£<?php echo $row["price"] ?></div>
                    <button class = "button" name="order" formaction='info.php' type="submit" value="<?php echo $row["id"] ?>">More Info</button>
                    <button class = "button" name="order" type="submit" value="<?php echo $row["id"] ?>">Order</button>
                </div>
        <?php } ?>

        </div>


        <?php
        echo "<div = 'pagnation_buttons'>";
        for ($i = 1; $i <= $total_pages; $i++) {
            echo "<a class = 'button' href='listart.php?page=" . $i . "'>" . $i . "</a>";
        }
        //Disconnect
        $conn->close();

        ?>


</div>
</form>
</body>
</html>