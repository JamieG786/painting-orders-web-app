# Painting Orders Web App

A basic web app for buying paintings using HTML, CSS, PHP and MySQL.
This was developed when I was learning about web developement as a coursework for the class CS312 at the University of Strathclyde.
This uses an account management system which is accessed using PHP and stored in MySQL.
The user information is hashed within the database for security purposes.
The database is what also stores all of the paintings which you can view on the website.

I have removed all the account information for MySQL from this code.

The website is available to view on the University of Strathclydes DevWeb server here : https://devweb2019.cis.strath.ac.uk/~rqb17144/listart.php/listart.php

![](PaintingOrdersScreenshot.png)
![](PaintingOrdersScreenshot2.png)
![](PaintingOrdersScreenshot3.png)
![](PaintingOrdersScreenshot4.png)