<!DOCTYPE html>
<html>
<head>
    <title>View Orders</title>
    <link rel="stylesheet" href="vieworders.css">
    <ul>
        <?php if (isset($_SESSION["loggedin"])) { ?>
            <li><a href="viewordersaccount.php">View Orders</a></li>
            <li><a href="logout.php">Log Out</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>

        <?php } else { ?>
            <li><a href="login.php">Sign In</a></li>
            <li><a href="createaccount.php">Sign Up</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>
        <?php } ?>
    </ul>
</head>
<body>


<h1>View Orders</h1>
<div>
    <form action="order.php" method="get">
        <?php
        //Connect to MySQL

        $conn = new mysqli($host, $user, $pass, $dbname);

        if ($conn->connect_error) {
            die("Connection Failed");
        }
        //Issue the query
        $sql = 'SELECT * FROM `orders`';
        $result = $conn->query($sql);

        if (!$result) {
            die("Query failed");
        }
        ?>

        <?php if ($result->num_rows > 0) { ?>
            <div class="grid">
                <?php while ($row = $result->fetch_assoc()) { ?>

                    <div class="item">
                        <div> Order ID : <span class="data"><?php echo $row["id"] ?> </span></div>
                        <div> Name : <span class="data"><?php echo $row["name"] ?> </span></div>
                        <div> Number : <span class="data"><?php echo $row["number"] ?> </span></div>
                        <div> Email : <span class="data"><?php echo $row["email"] ?> </span></div>
                        <div> Address : <span class="data"><?php echo $row["address"] ?> </span></div>
                        <div> Painting ID : <span class="data"><?php echo $row["painting_id"] ?> </span></div>
                        <div> Painting Name : <span class="data"><?php echo $row["painting_name"] ?> </span></div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
</div>

<?php
//Disconnect
$conn->close();
?>

</div>
</body>
</html>