<?php session_start(); ?>
<!DOCTYPE html>
<?php
//Connect to MySQL

$conn = new mysqli($host, $user, $pass, $dbname);

if ($conn->connect_error) {
    die("Connection Failed");
}


$email_err = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = isset($_POST['email']) ? $conn->real_escape_string($_POST['email']) : "";
    $emailsql = "SELECT * FROM `accounts` WHERE `email` ='$email'";
    $result = $conn->query($emailsql);
    if ($result->num_rows > 0) {

        while ($row = $result->fetch_assoc()) {

            if (password_verify($_POST["password"], $row["password"])) {
                $_SESSION["loggedin"] = "TRUE";
                $_SESSION["email"] = $email;
                header("location: listart.php");
            } else {
                $email_err = "Your email or password is incorrect";
            }
        }

    } else {
        $email_err = "Your email or password is incorrect";
    }
}


?>

<html>
<head>
    <title>Sign In</title>
    <link rel="stylesheet" href="login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <ul>
        <?php if (isset($_SESSION["loggedin"])) { ?>
            <li><a href="viewordersaccount.php">View Orders</a></li>
            <li><a href="logout.php">Log Out</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>

        <?php } else { ?>
            <li><a href="login.php">Sign In</a></li>
            <li><a href="createaccount.php">Sign Up</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>
        <?php } ?>
    </ul>
</head>
<body>
<h1>Sign In</h1>
<div class="grid">
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

        <div class="boxes">
            <input type="email" placeholder="Email" name="email" value="" required>
            <span class="error">* <?php echo $email_err ?></span>
            <br><br>
            <input type="password" placeholder="Password" name="password" required>
            <span class="error">*</span>
            <br><br>
            <button class = "submit" name="submit" type="Submit" value="">Log In</button>

    </form>
    <form action="forgotpassword.php">
        <button class = "submit" name="forgotpassword" type="Submit" value="">Forgot Password</button>
        <br><br>
    </form>
    <form action="listart.php">
        <button class = "submit" name="back" type="Submit" value="">Back</button>
    </form>
</div>
</div>
</body>
</html>