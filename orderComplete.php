<!DOCTYPE html>
  <?php
    //Connect to MySQL

    $conn = new mysqli($host, $user, $pass, $dbname);

    if ($conn->connect_error) {
        die("Connection Failed");
    }
  ?>

<html>
<head>
    <title>Order</title>
    <link rel="stylesheet" href="login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <ul>
        <?php if (isset($_SESSION["loggedin"])) { ?>
            <li><a href="viewordersaccount.php">View Orders</a></li>
            <li><a href="logout.php">Log Out</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>

        <?php } else { ?>
            <li><a href="login.php">Sign In</a></li>
            <li><a href="createaccount.php">Sign Up</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>
        <?php } ?>
    </ul>
</head>
<body>
<form action="listart.php">

    <?php
    $name = isset($_POST['name']) ? $conn->real_escape_string($_POST['name']) : "";
    $number = isset($_POST['number']) ? $conn->real_escape_string($_POST['number']) : "";
    $email = isset($_POST['email']) ? $conn->real_escape_string($_POST['email']) : "";
    $address = isset($_POST['address']) ? $conn->real_escape_string($_POST['address']) : "";
    $painting_id = isset($_POST['painting_id']) ? $conn->real_escape_string($_POST['painting_id']) : "";
    $painting_name = isset($_POST['painting_name']) ? $conn->real_escape_string($_POST['painting_name']) : "";



    $sql  = "INSERT INTO `orders` (`name`, `number`, `email`, `Address`, `painting_id`, `painting_name`)
             VALUES ('$name','$number','$email', '$address','$painting_id', '$painting_name')";
    $result = $conn->query($sql);

    if (!$result) {
        die("Query failed".$conn->error);
    }


    ?>
    <h1>Your order has been confirmed!</h1>
    <div class = "grid">
    <input class = "submit" type="submit" value="Back">
    </div>
</form>
</body>
</html>