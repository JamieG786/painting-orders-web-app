<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
    <title>Info</title>
    <link rel="stylesheet" href="info.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <ul>
        <?php if (isset($_SESSION["loggedin"])) { ?>
            <li><a href="viewordersaccount.php">View Orders</a></li>
            <li><a href="logout.php">Log Out</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>

        <?php } else { ?>
            <li><a href="login.php">Sign In</a></li>
            <li><a href="createaccount.php">Sign Up</a></li>
            <li class="title"><a href="listart.php">Art By Cara</a></li>
        <?php } ?>
    </ul>
</head>
<body>
<h1>More Information</h1>
<form action="order.php" method="get">
    <?php
    //Connect to MySQL

    $conn = new mysqli($host, $user, $pass, $dbname);

    if ($conn->connect_error) {
        die("Connection Failed");
    }
    $id = $conn->real_escape_string($_GET['order']);
    //Issue the query
    $sql = 'SELECT *  FROM `paintings` WHERE `id` = ' . $id;
    $result = $conn->query($sql);

    if (!$result) {
        die("Query failed");
    }
    //$row = $result->fetch_assoc();

    if ($result->num_rows > 0) { ?>


    <div class="grid">
        <?php while ($row = $result->fetch_assoc()) { ?>

            <div class="item">
                <div class="photo"> <?php echo "<img src='data:image/jpeg;base64," . base64_encode($row["image"]) . "'" ?> height = 50 width = 50>
                </div>
                <div class="painting_title"><?php echo $row["name"] ?> </div>
                <br>
                <div class="info">
                    <div>Date of Completion: <?php echo $row["date_of_completion"] ?></div>
                    <div>Height: <?php echo $row["height"] ?>cm</div>
                    <div>Width: <?php echo $row["width"] ?>cm</div>
                    <div class="painting_price">£<?php echo $row["price"] ?></div>
                    <div><?php echo $row["description"] ?></div>
                </div>
                <button class="button" name="order" formaction='listart.php' type="submit"
                        value="<?php echo $row["id"] ?>">Back
                </button>
                </button>
                <button class="button" name="order" type="submit" value="<?php echo $row["id"] ?>">Order</button>


            </div>


        <?php }
        } ?>

    </div>

    <?php
    //Disconnect
    $conn->close();
    ?>

</form>

</body>
</html>